package si.klen.AirHockey;

import static android.opengl.GLES20.*;


import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import si.klen.AirHockey.utils.LoggerConfig;
import si.klen.AirHockey.utils.ShaderHelper;
import si.klen.AirHockey.utils.TextResourceReader;


public class AirHockeyRenderer implements Renderer {

    private final Context context;
    private static final int  BYTES_PER_FLOAT = 4;
    private final FloatBuffer vertexData;

    private int program;
    int uColorLocation;
    int aPositionLocation;

    public AirHockeyRenderer(Context context){
        this.context = context;
        float[] tableVerticesWithTriangles = {
                // Triangle 1
                -0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f,
                // Triangle 2
                -0.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f,
                // Line 1
                -0.5f, 0f, 0.5f, 0f,
                // Mallets
                0f, -0.25f, 0f, 0.25f,
        };

        vertexData = ByteBuffer.allocateDirect(
                tableVerticesWithTriangles.length*BYTES_PER_FLOAT).order(
                        ByteOrder.nativeOrder()).asFloatBuffer();
        vertexData.put(tableVerticesWithTriangles);
    }
    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        String vertexShaderSource = TextResourceReader.readTextFileFromResource(context,
                R.raw.simple_vertext_shader);
        String fragmentShaderSource = TextResourceReader.readTextFileFromResource(context,
                R.raw.simple_fragment_shader);

        int vertexShader = ShaderHelper.compileVertexShader(vertexShaderSource);
        int fragmentShader = ShaderHelper.compileFragmentShader(fragmentShaderSource);

        program = ShaderHelper.linkProgram(vertexShader, fragmentShader);
        if(LoggerConfig.ON)
            ShaderHelper.validateProgram(program);

        glUseProgram(program);

        uColorLocation = glGetUniformLocation(program, "u_Color");
        aPositionLocation = glGetAttribLocation(program, "a_Position");

        vertexData.position(0);
        glVertexAttribPointer(aPositionLocation,2,GL_FLOAT,
                false, 0, vertexData);

        glEnableVertexAttribArray(aPositionLocation);

    }

    /**
     * onSurfaceChanged is called whenever the surface has changed. This is
     * called at least once when the surface is initialized. Keep in mind that
     * Android normally restarts an Activity on rotation, and in that case, the
     * renderer will be destroyed and a new one created.
     *
     * @param width
     *            The new width, in pixels.
     * @param height
     *            The new height, in pixels.
     */
    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        // Set the OpenGL viewport to fill the entire surface.
        glViewport(0, 0, width, height);
    }

    /**
     * OnDrawFrame is called whenever a new frame needs to be drawn. Normally,
     * this is done at the refresh rate of the screen.
     */
    @Override
    public void onDrawFrame(GL10 glUnused) {
        // Clear the rendering surface.
        glClear(GL_COLOR_BUFFER_BIT);

        glUniform4f(uColorLocation,1f,1f,1f,1f);
        glDrawArrays(GL_TRIANGLES,0,6);

        glUniform4f(uColorLocation,1f,0,0,1f);
        glDrawArrays(GL_LINES,6,2);

        glDrawArrays(GL_POINTS,8,1);

        //glUniform1f(uSizeLocation,5f);
        glUniform4f(uColorLocation,0f,0,1f,1f);
        glDrawArrays(GL_POINTS,9,1);

    }
}