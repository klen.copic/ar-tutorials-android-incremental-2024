/*===============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.vuforia.engine.Video.app.VideoPlayback;

class VideoPlaybackShaders
{
    
    public static final String VIDEO_PLAYBACK_VERTEX_SHADER = " \n"
        + "attribute vec4 vertexPosition; \n"
        + "attribute vec2 vertexTexCoord; \n"
        + "varying vec2 texCoord; \n"
        + "uniform mat4 modelViewProjectionMatrix; \n"
        + "\n"
        + "void main() \n"
        + "{ \n"
        + "   gl_Position = modelViewProjectionMatrix * vertexPosition; \n"
        + "   texCoord = vertexTexCoord; \n"
        + "} \n";

    /*
     *
     * IMPORTANT:
     *
     * The SurfaceTexture functionality from ICS provides the video frames from
     * the movie in an unconventional format. So we cant use Texture2D but we
     * need to use the ExternalOES extension.
     *
     * Two things that are important in the shader below. The first is the
     * extension declaration (first line). The second is the type of the
     * texSamplerOES uniform.
     */

    //YOU NEED TO CAHNGE or ADD CODE IN THIS FRAGMENT SHADER
    public static final String VIDEO_PLAYBACK_FRAGMENT_SHADER = " \n"
        + "#extension GL_OES_EGL_image_external : require \n"
        + "precision mediump float; \n"
        + "varying vec2 texCoord; \n"
        + "vec4 out1; \n"
        + "uniform samplerExternalOES texSamplerOES; \n" + " \n"
        + "void main() \n"
        + "{ \n"
            + "   out1 = texture2D(texSamplerOES, texCoord);\n"
            +"if(out1[0]<0.1 && out1[1]>0.9 && out1[2]<0.1){"
            +"     gl_FragColor = vec4(1,1,1,0);"
            +"}else{"
            +"   gl_FragColor = texture2D(texSamplerOES, texCoord);\n"
            +"}"
        + "} \n";

    /*
     *
     * IMPORTANT:
     *
     * The SurfaceTexture functionality from ICS provides the video frames from
     * the movie in an unconventional format. So we cant use Texture2D but we
     * need to use the ExternalOES extension.
     *
     * Two things that are important in the shader below. The first is the
     * extension declaration (first line). The second is the type of the
     * texSamplerOES uniform.
     */

//    public static final String VIDEO_PLAYBACK_FRAGMENT_SHADER = " \n"
//            + "#extension GL_OES_EGL_image_external : require \n"
//            + "precision mediump float; \n"
//            + "varying vec2 texCoord; \n"
//            + "uniform sampler2D texSampler2DMask;\n"
//            + "vec4 mask;\n"
//            + "float t = 0.1;\n"
//
//            + "uniform samplerExternalOES texSamplerOES; \n" + " \n"
//            + "void main() \n"
//            + "{ \n"
//            +"    gl_FragColor = texture2D(texSamplerOES, texCoord); \n"
//            +"    mask = texture2D(texSamplerOES, texCoord); \n"
//            + "   if(t>mask[0] && (1.0-t)<mask[1] && t>mask[2] )"
//            + "         gl_FragColor = vec4(1f, 1f, 1f, 0f); \n"
//            + "} \n";

    
}
